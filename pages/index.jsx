import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import axios from "axios";

const Home = ({ data }) => {
  const items = data?.products;

  return (
    <div data-test="IndexPage">
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>{`Dishwashers (${items?.length})`}</h1>
        <div className={styles.content}>
          {items &&
            items?.slice(0, 20)?.map((item) => (
              <div key={item?.productId} className={styles?.item}>
                <Link
                  href={{
                    pathname: "/product-detail/[id]",
                    query: { id: item.productId },
                  }}
                >
                  <a className={styles.link}>
                    <ProductListItem
                      image={item?.image}
                      price={item?.variantPriceRange?.display?.min}
                      description={item?.title}
                    />
                  </a>
                </Link>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const response = await axios.get(
    // API_KEY = AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI stored in .env
    // expose .env since this is not a real page
    `https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=${process.env.API_KEY}`
  );
  const data = response?.data || {};

  return {
    props: { data },
  };
}

export default Home;
