import axios from "axios";
import Head from "next/head";
import Link from "next/link";
import styles from "./product-detail.module.scss";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import ProductOrderPanel from "../../components/product-order-panel/product-order-panel";
import ProductDescription from "../../components/product-description/product-description";
import ProductSpecification from "../../components/product-specification/product-specification";

const ProductDetail = ({ data }) => {
  return (
    <div data-test="ProductDetailsPage">
      <Head>
        <title>{`JL & Partners | ${data?.title}`}</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div className={styles.header}>
        <Link
          href={{
            pathname: "/",
          }}
        >
          <img
            style={{ height: "30px", cursor: "pointer" }}
            src="/leftarrow.svg"
          />
        </Link>
        <h1>
          <div dangerouslySetInnerHTML={{ __html: data?.title }} />
        </h1>
        <div />
      </div>
      <div className={styles.content}>
        <div className={styles.leftbox}>
          {data?.media && <ProductCarousel images={data?.media?.images} />}
        </div>

        <ProductOrderPanel
          price={`£${data?.price?.now || ""}`}
          offer={data?.displaySpecialOffer || ""}
          service={data?.additionalServices?.includedServices || ""}
        />

        <div className={styles.description}>
          <ProductDescription
            code={data?.code || ""}
            description={data?.details?.productInformation || ""}
          />

          <ProductSpecification
            features={data?.details?.features[0]?.attributes || []}
          />
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await axios.get(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  );
  const data = (await response?.data) || {};

  return {
    props: { data },
  };
}

export default ProductDetail;
