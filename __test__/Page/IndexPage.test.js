import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import IndexPage from "pages/index.jsx";

const setup = (props = {}, state = null) => {
  return shallow(<IndexPage {...props} />);
};

test("check if IndexPage runs successfully", () => {
  const wrapper = setup();
});

test("check if IndexPage runs successfully", () => {
  const wrapper = setup();
  const IndexPage = findJSXByAttr(wrapper, "IndexPage");
  expect(IndexPage.length).toBe(1);
});
