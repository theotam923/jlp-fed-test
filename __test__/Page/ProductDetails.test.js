import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductDetailsPage from "pages/product-detail/[id].jsx";

const setup = (props = {}, state = null) => {
  return shallow(<ProductDetailsPage {...props} />);
};

test("check if ProductDetailsPage runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductDetailsPage runs successfully", () => {
  const wrapper = setup();
  const ProductDetailsPage = findJSXByAttr(wrapper, "ProductDetailsPage");
  expect(ProductDetailsPage.length).toBe(1);
});
