import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductListItem from "components/product-list-item/product-list-item";

const setup = (props = {}, state = null) => {
  return shallow(<ProductListItem {...props} />);
};

test("check if ProductListItem runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductListItem runs successfully", () => {
  const wrapper = setup();
  const ProductListItem = findJSXByAttr(wrapper, "ProductListItem");
  expect(ProductListItem.length).toBe(1);
});
