import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductDescription from "components/product-description/product-description";

const setup = (props = {}, state = null) => {
  return shallow(<ProductDescription {...props} />);
};

test("check if ProductDescription runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductDescription runs successfully", () => {
  const wrapper = setup();
  const ProductDescription = findJSXByAttr(wrapper, "ProductDescription");
  expect(ProductDescription.length).toBe(1);
});
