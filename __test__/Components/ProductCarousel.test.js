import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductCarousel from "components/product-carousel/product-carousel";

const setup = (props = {}, state = null) => {
  return shallow(<ProductCarousel {...props} />);
};

test("check if ProductCarousel runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductCarousel runs successfully", () => {
  const wrapper = setup();
  const ProductCarousel = findJSXByAttr(wrapper, "ProductCarousel");
  expect(ProductCarousel.length).toBe(1);
});
