import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductSpecification from "components/product-specification/product-specification";

const setup = (props = {}, state = null) => {
  return shallow(<ProductSpecification {...props} />);
};

test("check if ProductSpecification runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductSpecification runs successfully", () => {
  const wrapper = setup();
  const ProductSpecification = findJSXByAttr(wrapper, "ProductSpecification");
  expect(ProductSpecification.length).toBe(1);
});
