import React from "react";
import { shallow } from "enzyme";
import { findJSXByAttr } from "__test__/testUtils";

import ProductOrderPanel from "components/product-order-panel/product-order-panel";

const setup = (props = {}, state = null) => {
  return shallow(<ProductOrderPanel {...props} />);
};

test("check if ProductOrderPanel runs successfully", () => {
  const wrapper = setup();
});

test("check if ProductOrderPanel runs successfully", () => {
  const wrapper = setup();
  const ProductOrderPanel = findJSXByAttr(wrapper, "ProductOrderPanel");
  expect(ProductOrderPanel.length).toBe(1);
});
