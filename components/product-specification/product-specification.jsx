import styles from "./product-specification.module.scss";

const ProductSpecification = ({ features }) => {
  return (
    <div data-test="ProductSpecification">
      <div className={styles.item}>
        <h1 className={styles.heading}>Product specification</h1>
      </div>
      {features &&
        features?.map((item, idx) => (
          <div key={`specification_${idx}`} className={styles.specification}>
            <div>{item?.name}</div>
            <div style={{ textAlign: "right" }}>{item?.value}</div>
          </div>
        ))}
    </div>
  );
};

export default ProductSpecification;
