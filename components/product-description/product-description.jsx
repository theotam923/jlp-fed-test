import styles from "./product-description.module.scss";
import { useState } from "react";

const ProductDescription = ({ code, description }) => {
  const WORDS_LIMIT = 250;
  const [fullDesc, setFullDesc] = useState(false);
  const isShortDesc = description?.length <= WORDS_LIMIT;

  const togggleDescription = () => {
    setFullDesc(!fullDesc);
  };

  return (
    <div data-test="ProductDescription">
      <div className={styles.item}>
        <h1 className={styles.heading}>Product information</h1>
        <div>Product code: {code}</div>
        {isShortDesc || fullDesc ? (
          <div
            dangerouslySetInnerHTML={{
              __html: description,
            }}
          />
        ) : (
          <div
            dangerouslySetInnerHTML={{
              __html: description?.slice(0, WORDS_LIMIT) + "...",
            }}
          />
        )}
      </div>
      {!isShortDesc && (
        <div className={styles?.section}>
          <div
            style={{ fontWeight: "400", cursor: "pointer" }}
            onClick={() => togggleDescription()}
          >
            {fullDesc ? " Read less" : "Read more"}
          </div>
          <div style={{ float: "right" }}>
            <img
              onClick={() => togggleDescription()}
              style={{
                height: "25px",
                transform: "scaleX(-1)",
                cursor: "pointer",
              }}
              align="right"
              src="/leftarrow.svg"
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductDescription;
