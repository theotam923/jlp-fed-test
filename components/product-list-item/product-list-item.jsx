import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description }) => {
  return (
    <div data-test='ProductListItem' className={styles.content}>
      <div>{image && <img src={image} alt="" style={{ width: "100%" }} />}</div>
      <div>{description || ""}</div>
      <div className={styles.price}>{price || ""}</div>
    </div>
  );
};

export default ProductListItem;
