import styles from "./product-order-panel.module.scss";

const ProductOrderPanel = ({ price, offer, service }) => {
  return (
    <div
      data-test="ProductOrderPanel"
      className={styles.orderPanel}
      aria-pretend="true"
    >
      <h1 className={styles.price}>{price}</h1>
      <h3 className={styles.offer}>{offer}</h3>
      <h3 className={styles.service}>{service}</h3>
    </div>
  );
};

export default ProductOrderPanel;
