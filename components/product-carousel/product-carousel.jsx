import { useState } from "react";
import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ images }) => {
  const [show, setShow] = useState(0);

  const handleImage = (idx) => {
    setShow(idx);
  };

  return (
    <>
      <div
        data-test="ProductCarousel"
        className={styles.productCarousel}
        aria-pretend="true"
      >
        {images && (
          <img
            src={images?.urls[show]}
            alt=""
            style={{ width: "100%", maxWidth: "500px" }}
          />
        )}
      </div>
      <div className={styles.navigation}>
        {images?.urls.map((_, idx) => {
          return (
            <span
              key={`image_${idx}`}
              style={{ color: show === idx ? "#000" : "#999999" }}
              onClick={() => handleImage(idx)}
            >
              .
            </span>
          );
        })}
      </div>
    </>
  );
};

export default ProductCarousel;
